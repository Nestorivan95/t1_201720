package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations < T extends Number > 
{		

	public double computeMean(IntegersBag bag){
		double mean = 0;
		double length = 0;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next().doubleValue();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public double getMax(IntegersBag bag)
	{
		double max = Double.MIN_VALUE;
		double value;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				if( max < value){
					max = value;
				}
			}
		}
		return max;
	}

	public double getMin( IntegersBag bag)
	{
		double min = Double.MAX_VALUE; 
		double value; 

		if( bag != null)
		{
			Iterator<T> iter = bag.getIterator(); 

			while( iter.hasNext() )
			{
				value = iter.next().doubleValue(); 

				if( min > value )
				{
					min = value; 					
				}
			}
		}
		return min; 
	}

	public double getSuma( IntegersBag bag)
	{
		double suma = 0; 
		double number; 
		ArrayList<Double> numeros = new ArrayList<Double>();

		if( bag != null )
		{
			Iterator<T> iter = bag.getIterator(); 

			while( iter.hasNext() )
			{
				number = iter.next().doubleValue(); 

				if( number != 0)
				{
					numeros.add(number); 
				}					

				for( Double i: numeros)
				{
					suma = suma + i; 					
				}
			}
		}

		return suma;
	}

	public double getMedia( IntegersBag bag)
	{
		double number; 
		int suma = 0;
		double media = 0; 
		ArrayList<Double> numeros = new ArrayList<Double>();

		if( bag != null )
		{
			Iterator<T> iter = bag.getIterator(); 

			while( iter.hasNext() )
			{
				number = iter.next().doubleValue(); 

				if(number != 0)
				{       
					numeros.add(number);
					suma += number; 
					media = suma/ numeros.size(); 
				}
			}
		}
		
		return media; 
	}
	
}
